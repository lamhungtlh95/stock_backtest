import datetime
from logging import debug
import pandas as pd
from flask import jsonify
from datetime import datetime, timedelta


def handle(data_file, start_date, end_date, in_sample_period, out_sample_period, num_of_ouput):
    final_data= []
    data = pd.read_csv(data_file).dropna(axis=1, how='all')

    #Drop cash columns
    for col in data.columns:
        if len(data[col].unique()) == 1:  # Find unique values in column along with their length and if len is == 1 then it contains same values
            data.drop([col], axis=1, inplace=True)  # Drop the column

    in_sample_period = in_sample_period
    out_sample_period = out_sample_period
    num_of_ouput = num_of_ouput
    start_date = start_date
    end_date = end_date

    start_date_pares = datetime.strptime(start_date, '%m/%d/%Y')
    end_date_parse = datetime.strptime(end_date, '%m/%d/%Y')

    #data_from_day_to_day: list data period from day to day picked by customer
    data_from_day_to_day = pd.DataFrame(columns=data.columns.tolist())
    index = 0
    for r in data.iterrows():
        data_date_parse = datetime.strptime(r[1][0], '%m/%d/%Y')
        if data_date_parse >= start_date_pares and data_date_parse <= end_date_parse:
            data_from_day_to_day.loc[index] = r[1].values.tolist()
            index+=1


    start_period_date = 0
    lookback_period_date = in_sample_period
    list_data_period = pd.DataFrame(columns=data.columns.tolist())

    period_index = 0
    while_loop = 0
    while while_loop == 0:
        #Handle ROR
        list_data_period2 = []
        for row in data_from_day_to_day.index[start_period_date:lookback_period_date]:
            if row >= start_period_date and row <=lookback_period_date:
                # import pdb; pdb.set_trace()
                arrr = (data_from_day_to_day.loc[row]).values.tolist()
                list_data_period2.append(arrr)
                period_index+=1
        # import pdb; pdb.set_trace()
        list_data_period2 = pd.DataFrame(list_data_period2, columns=data.columns.tolist())
        for it in list_data_period2.index[0: len(list_data_period2)-1]:
            list_stock = []
            stock_percent = 100/num_of_ouput
            left_percent = 100
            for col in data.columns.tolist():
                if col == 'Date':
                    list_stock.append(list_data_period2.loc[it][0])
                else:
                    values = list_data_period2[col].tolist()
                    ror = (values[0] / values[-1]) -1
                    list_stock.append(ror)

            top_largest = sorted(list_stock[1: len(list_stock)], reverse=True)
            top_largest = top_largest[0: num_of_ouput]

            list_stock.append(left_percent)

            for largest_item in top_largest:
                if largest_item <= 0:
                    top_largest.remove(largest_item)

            for idx, item in enumerate(list_stock[1: len(list_stock)-1]):
                if item >= 0 and item in top_largest:
                    list_stock[idx+1] = stock_percent
                    left_percent = left_percent - stock_percent
                    list_stock[len(list_stock)-1] = left_percent
                else:
                    list_stock[idx+1] = 0

            final_data.append(list_stock)

        lookback_period_date = lookback_period_date + out_sample_period
        start_period_date = lookback_period_date - in_sample_period

        if lookback_period_date > len(data_from_day_to_day):
            while_loop+=1
    
    final_data_columns = data.columns.tolist()
    final_data_columns.append('Cash')
    final_data = pd.DataFrame(final_data, columns=final_data_columns)
    final_data.to_csv('data_output_data.csv')
    return final_data


if __name__ == '__main__':
    # app.run(debug=True)
    handle('Stocks.csv', '7/3/2006', '8/20/2007', 10, 5, 4)
